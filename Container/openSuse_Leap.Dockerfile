FROM opensuse/leap:15.1

ADD ./Scripts /Scripts

RUN sh /Scripts/openSUSELeap15/InstallPackages.sh       && \
    sh /Scripts/UpdateAlternatives.sh                   && \
    sh /Scripts/InstallPipPackages.sh                   && \
    sh /Scripts/InstallCMake.sh                         && \
    sh /Scripts/InstallBreakpad.sh                      && \
    sh /Scripts/InstallOpenSSL.sh                       && \
    sh /Scripts/InstallBoost.sh                         && \
    sh /Scripts/openSUSELeap15/InstallPowershell.sh     && \
    rm -rf /Scripts
