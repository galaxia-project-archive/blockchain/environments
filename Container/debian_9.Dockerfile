FROM debian:9

ADD ./Scripts /Scripts

RUN sh /Scripts/debian9/AddBusterPPA.sh                 && \
    sh /Scripts/Ubuntu/InstallPackages.sh               && \
    sh /Scripts/InstallPipPackages.sh                   && \
    sh /Scripts/UpdateAlternatives.sh                   && \
    sh /Scripts/InstallCMake.sh                         && \
    sh /Scripts/InstallBreakpad.sh                      && \
    sh /Scripts/InstallOpenSSL.sh                       && \
    sh /Scripts/InstallBoost.sh                         && \
    sh /Scripts/debian9/InstallPowershell.sh            && \
    rm -rf /Scripts
