#!/bin/bash

set -e

echo "deb http://ftp.de.debian.org/debian/ buster main contrib non-free" >> /etc/apt/sources.list
echo "deb-src http://ftp.de.debian.org/debian/ buster main contrib non-free" >> /etc/apt/sources.list

set +e
