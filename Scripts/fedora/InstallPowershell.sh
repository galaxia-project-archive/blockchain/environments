#!/bin/bash

set -e

rpm --import https://packages.microsoft.com/keys/microsoft.asc

wget -q -O - https://packages.microsoft.com/config/rhel/7/prod.repo | tee /etc/yum.repos.d/microsoft.repo

dnf update -y
dnf install -y powershell
dnf clean all

set +e