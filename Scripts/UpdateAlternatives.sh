#!/bin/bash

set -e

update-alternatives --install /usr/bin/cc cc /usr/bin/gcc-8 100
update-alternatives --install /usr/bin/c++ c++ /usr/bin/g++-8 100
update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-8 100
update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-8 100
update-alternatives --install /usr/bin/gcc-ar gcc-ar /usr/bin/gcc-ar-8 100
update-alternatives --install /usr/bin/gcc-ranlib gcc-ranlib /usr/bin/gcc-ranlib-8 100

set +e