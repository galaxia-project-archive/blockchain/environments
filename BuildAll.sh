#!/bin/bash

set -e

function buildContainer {
    CONTAINER_BASE=$1
    CONTAINER_IMAGE=$2
    CONTAINER_FILE=$3
    

    docker pull $CONTAINER_BASE || true
    docker pull $CONTAINER_IMAGE || true
    docker build --tag $CONTAINER_IMAGE --file ./Container/$CONTAINER_FILE .    
    # docker push $CONTAINER_IMAGE
}

buildContainer \
    opensuse/leap:15.1 \
    registry.gitlab.com/galaxia-project/environment/opensuse:leap \
    openSuse_Leap.Dockerfile

exit 0

buildContainer \
    ubuntu:bionic \
    registry.gitlab.com/galaxia-project/environment/ubuntu:bionic \
    Ubuntu_Bionic.Dockerfile

buildContainer \
    ubuntu:xenial \
    registry.gitlab.com/galaxia-project/environment/ubuntu:xenial \
    Ubuntu_Xenial.Dockerfile

buildContainer \
    centos:7 \
    registry.gitlab.com/galaxia-project/environment/centos:7 \
    centOS_7.Dockerfile

buildContainer \
    fedora:29 \
    registry.gitlab.com/galaxia-project/environment/fedora:29 \
    fedora_29.Dockerfile

buildContainer \
    debian:9 \
    registry.gitlab.com/galaxia-project/environment/debian:9 \
    debian_9.Dockerfile    

buildContainer \
    python \
    registry.gitlab.com/galaxia-project/environment/cpplint:latest \
    CppLint.Dockerfile

buildContainer \
    registry.gitlab.com/galaxia-project/environment/ubuntu:bionic \
    registry.gitlab.com/galaxia-project/environment/cppcheck:latest \
    CppCheck.Dockerfile

set +e

